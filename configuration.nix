{ config, pkgs, lib, ... }:

let home-manager = builtins.fetchGit {
		url = "https://github.com/rycee/home-manager.git";
		rev = "8b15f1899356762187ce119980ca41c0aba782bb";
		ref = "master";
	};

	secrets = import ./secrets.nix;

in 

{
	imports = [
		./hardware-configuration.nix
		"${home-manager}/nixos"
	];

	fileSystems."/mnt" = {
		device = "/dev/sda1";
		fsType = "ext4";
		options = [
			"nofail"
			"umask=0755"
			"uid=diamond"
			"gid=users"
		];
	};

	services.sshd.enable = true;

	networking.hostName = "hime5to";
	networking.enableIPv6 = false;
	networking.wireless = {
		enable = true;
		interfaces = [ "wlan0" ];
		networks = secrets.wirelessNetworks;
	};
	networking.nameservers = [ "1.1.1.1" ];
	networking.defaultGateway = {
		address = secrets.networkAddr.gateway;
		interface = "wlan0";
	};
	networking.interfaces.wlan0.ipv4 = {
		addresses = [{
			address = secrets.networkAddr.address;
			prefixLength = 24;
		}];
	};
	networking.firewall = {
		allowedTCPPorts = [ 22 ];
	};

	time.timeZone = "America/Los_Angeles";

	environment.systemPackages = with pkgs; [
		lm_sensors
		neovim
		wget
		git
	];

	users.extraUsers.root.openssh.authorizedKeys.keys = secrets.sshKeys;

	users.users.diamond = {
		isNormalUser = true;
		extraGroups = [ "wheel" ];
		# password
		initialHashedPassword = secrets.defaultPassword;
		openssh.authorizedKeys.keys = secrets.sshKeys;
	};

	system.stateVersion = "20.03";
}
